/*
    Description:
        Gives out inspirational quotes from Party approved role models

    Configuration:
        FAVORITE_LEADER - An optional preferred leader to draw quotes from

    Commands:
        party approved - provides list of Party approved role models
        enlighten - provides a quote from one of the members of the party
*/

module.exports = function (kombot){
    var leaders = require('./kommy-quotes.json');
    kombot.hear('enlighten', function(msg){
        var leader = leaders[0],
            curQuote = Math.floor(Math.random() * (leader.quotes.length));
        ;
        msg.send(leader.quotes[curQuote]);
    });

    kombot.hear('party approved', function(msg){
        leaders.forEach(function(leader){
            msg.send('Komrade ' + leader.name + ' is strong like Soviet bear');
        })
    });

};
